<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>

<?php
/*
 afficher les tables de multiplications de 0 à 10 dans un tableau html
*/
$a = 0;

?>
<style>
    table{
        background-color: lightblue;
        color:white;
    }
    td{
        border: 1px solid black; 
        padding:10px;
    }
</style>
<!-- écrire le code après ce commentaire -->

<table>
    <tr>
        <td>...</td>
        <td>0</td>
        <td>1</td>
        <td>2</td>
        <td>3</td>
        <td>4</td>
        <td>5</td>
        <td>6</td>
        <td>7</td>
        <td>8</td>
        <td>9</td>
    </tr>


</table>

<!-- écrire le code avant ce commentaire -->

</body>
</html>

