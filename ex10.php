<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
</head>
<body>
<?php
/*
 Afficher 50 div de couleures aléatoires (utiliser rgb: https://www.w3schools.com/cssref/func_rgb.asp)
*/

?>

<!-- écrire le code après ce commentaire -->
<style>
    div{
        width:50px;
        height:50px;
    }
</style>


<?php
    

    $x = 0;
    
    while ($x <=50){
        $a = rand (0, 255);
        $b = rand (0, 255);
        $c = rand (0, 255);
        echo "<div style = 'background-color:rgb($a, $b, $c); float:left;'></div>";
        $x++;
    }

?>


<!-- écrire le code avant ce commentaire -->

</body>
</html>

