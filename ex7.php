<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Exercice PHP</title>
    <style>
    #result-box{
        width: 50px;
        height: 50px;
        border: 1px solid black
    }
    </style>
</head>
<body>
<div id="result-box"></div>
<?php
$a = rand(0,20);
/*
 si $a est inferieur à 5, colorer la div #result-box en rouge, 
 si $a est compris entre 5 et 10, colorer la div #result-box en orange, 
 si $a est compris entre 11 et 15, colorer la div #result-box en jaune, 
 si $a est superieur à 15, colorer la div #result-box en vert, 
*/
?>
<!-- écrire le code après ce commentaire -->
<?php
    echo $a;
    if($a <5){
        echo "<style> #result-box {background-color: red;}</style>";
    }elseif ($a > 5 && $a <10) {
        echo "<style> #result-box {background-color: orange;}</style>";
    }elseif ($a > 11 && $a <15){
        echo "<style> #result-box {background-color: yellow;}</style>";
    }else {
        echo "<style> #result-box {background-color: green;}</style>";
    }


?>


<!-- écrire le code avant ce commentaire -->

</body>
</html>

